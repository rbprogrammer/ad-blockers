#!/usr/bin/env bash

# Ensure the cwd is at the project level.
cd $(dirname ${BASH_SOURCE[@]})/../

HOSTS_FILE="./etc/hosts.ads"
SORTED_HOSTS_FILE="${HOSTS_FILE}.sorted"
DISABLED_IP=0.0.0.0

function header() {
  echo
  echo '========================================'
  echo ${@}
}

header 'adding hosts'
for host in "${@}" ; do
  echo "${DISABLED_IP} ${host}" | tee -a ${HOSTS_FILE}
done

header 'sorting hosts'
sort --unique "--output=${SORTED_HOSTS_FILE}" "${HOSTS_FILE}"

header 'saving sorted hosts'
mv -vf "${SORTED_HOSTS_FILE}" "${HOSTS_FILE}"

header 'pushing changes to remote repo'
git add "${HOSTS_FILE}"
git commit -m 'Saving more hosts to the dnsmasq ad blocker.'
git pull
git push
