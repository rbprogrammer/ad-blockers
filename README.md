# dnsmasq

The `hosts.ads` file can be used much like `/etc/hosts` to block websites of
known ad websites.

If running DD-WRT on the router, and the router is set up as a DNS (i.e. running
something like dnsmasq) then putting this script into the `/tmp/.rc_startup`
script would automatically add the `hosts.ads` file to dnsmasq as an extra hosts
file.

To install the startup script, run these commands on a DD-WRT router (or
anything running dnsmasq):
```bash
STARTUP_FILE=.rc_startup
URL=https://gitlab.com/rbprogrammer/ad-blockers/raw/master/dnsmasq/bin/${STARTUP_FILE}
curl -k -o /tmp/${STARTUP_FILE} ${URL}
```

# google-chrome

Essentially this project hides those pesky Google ads on Google search pages
that look like legit search results but are just ads.

The main reason for this project is two-fold: (1) I hate ads... they annoy the
crap out of me, and (2) I run my own DNS (with an
"[ad-blocker](https://github.com/rbprogrammer/ad-blocker)") to disable many ad
hosting servers. However, since the ads that Google makes appear like search
results cannot be disabled through hijacking the IP/hostname of the server.
Hence this Chrome extension.
