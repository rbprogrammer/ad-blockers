// ==UserScript==
// @name        Hide ad links on search pages.
// @namespace   ads
// @version     1.0
// @description Hide those annoying af sponsored ad links on search pages.
// @author      You
// @match       https://duckduckgo.com/*
// @match       https://www.google.com/search*
// @updateURL   https://gitlab.com/rbprogrammer/ad-blockers/-/raw/master/firefox/ad-hider.user.js
// @downloadURL https://gitlab.com/rbprogrammer/ad-blockers/-/raw/master/firefox/ad-hider.user.js
// @grant       none
// ==/UserScript==

(function() {
    'use strict';
    let elements = [];

    // DuckDuckGo.
    elements = elements.concat(document.querySelectorAll('.results--ads'));

    // Google.
    elements.push(document.getElementById('tads'));

    //console.log(elements);
    //console.log(document.querySelectorAll('.ads-ad'));

    // Disable those fuckers.
    for (let el of elements) {
        for (let node of el) {
            node.style.display = 'none';
        }
    }
})();
